variable "DO_TOKEN" {}
variable "DO_PRIVKEY" {}
variable "DO_KEYFINGERPRINT" {}
variable "DO_REGION" {}
variable "DO_SIZE" {}
variable "DO_MASTERCOUNT" {}
variable "DO_WORKERCOUNT" {}
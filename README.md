# codinghaus_20181023_automatedSwarmModeClusterWithTraefik

example code for my blog post: How-To: Use Traefik as reverse proxy for your Docker Swarm Mode cluster on DigitalOcean (fully automated with GitLab CI, terraform, ansible) (https://codinghaus.de/2018/09/24/how-to-use-traefik-as-reverse-proxy-for-your-docker-swarm-mode-cluster-on-digitalocean-fully-automated-with-gitlab-ci-terraform-ansible/)

.infrastructure folder contains the infrastructure code (terraform)

docker-compose.yml describes our services which should run on our swarm mode cluster

.gitlab-ci.yml describes what our pipeline should do and when it should be executed

traefik.toml contains the configuration for the traefik reverse proxy

To try the pipeline with gitlab CI create the needed environment variables in gitlab as described in the blog post:

TF_VAR_DO_TOKEN - your DigitalOcean token (my DigitalOcean token :P)

TF_VAR_DO_PRIVKEY - your private ssh key (my private ssh key :P)

TF_VAR_DO_KEYFINGERPRINT - Your public ssh key's fingerprint. You have to add that public key to DigitalOcean in your DigitalOceans' account settings. (my key fingerprint :P)

TF_VAR_DO_REGION - the region where you want to create the droplet (fra1, which is Frankfurt Germany)

TF_VAR_DO_SIZE - the size your droplets should have (s-1vcpu-1gb, which is the smallest - 5$ per month)

TF_VAR_DO_MASTERCOUNT - count of your swarm manager nodes (3)

TF_VAR_DO_WORKERCOUNT - count of your swarm worker nodes (3)
